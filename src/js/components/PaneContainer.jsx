import { h, Fragment, Component } from 'preact';

export default class PaneContainer extends Component {
    render() {
		const { ...props } = this.props;
        const { left, right, footer } = props;  

        const leftElements = left.map((Component, index) => (
        <Component key={index} />
        ));

        const rightElements = right.map((Component, index) => (
        <Component key={index} />
        ));

        const footerElements = footer.map((Component, index) => (
        <Component key={index} />
        ));
        
        return (
            <Fragment>
                <div class="pane-container">
                    <div className="split left-pane">
                        {leftElements}
                    </div>
                    <div className="split right-pane">
                        {rightElements}
                    </div>
                </div>
                <footer>
                    {footerElements}
                </footer>
            </Fragment>
        );
    };
}