import { IPlugin } from './IPlugin';
import { EventManager } from './EventManager';
import { IPanes } from './IPanes';

export class PluginBase implements IPlugin {
	private _eventManager: EventManager;
	protected get eventManager(): EventManager {
		return this._eventManager;
	}

	setEventManager(eventManager: EventManager): void {
		this._eventManager = eventManager;
	}

	registerUIElement(panes: IPanes): void {
	}

	start(): void {
		this._eventManager.subscribe('Core.Init', () => this.init());
	}

	init(): void {
		
	}
}