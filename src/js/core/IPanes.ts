import { Component } from "preact";

export interface IPanes {
    left: typeof Component[];
    right: typeof Component[];
    footer: typeof Component[];
}