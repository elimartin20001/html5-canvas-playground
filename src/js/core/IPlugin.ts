import { EventManager } from "./EventManager";

export interface IPlugin {
    setEventManager(eventManager: EventManager): void;

    registerUIElement(panes: any): void;

    start(): void;
}