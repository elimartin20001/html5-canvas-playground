import { Core } from './core/Core';
import { CodeEditor } from './plugins/codeeditor/CodeEditor';
import { CodeStorage } from './plugins/codestorage/CodeStorage';
import { CanvasPreview } from './plugins/canvaspreview/CanvasPreview';
import { Layout } from './plugins/layout/Layout';
import { AutoSave } from './plugins/autosave/Autosave';
import { Footer } from './plugins/footer/Footer';
import App from './components/app.jsx';
import { h, render } from 'preact';

import '../css/style.css';

window.onload = () => {
	const core: Core = new Core();
	core.registerPlugins([
		new Layout(),
		new CodeEditor(),
		new CodeStorage(),
		new CanvasPreview(),
		new AutoSave(),
		new Footer()
	]);

	render(h(App, core.panes), document.body);

	core.init();
};
