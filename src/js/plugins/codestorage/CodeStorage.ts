import { PluginBase } from '../../core/PluginBase';

export class CodeStorage  extends PluginBase {
	init(): void {
		this.eventManager.subscribe('CodeEditor.SaveCode', (e: any) => this.saveCode(e));

		const lastCode = window.localStorage.getItem('code');
		if (lastCode) {
			this.eventManager.publish('CodeEditor.InitCode', { code: lastCode });
		}
	}

	saveCode(e: any) {
		window.localStorage.setItem('code', e.code);

		const message: string = e.type === 'autosave' ? 'Autosaved!' : 'Saved!';
		this.eventManager.publish('Core.StatusBarMessage', { message: message });

		setTimeout(() => {
			this.eventManager.publish('Core.StatusBarMessage', { message: '' });
		}, 1000);
	}
}