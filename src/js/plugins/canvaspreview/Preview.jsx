import { h, Component } from 'preact';

export default class Preview extends Component {
    render() {
        return (
            <div id="preview">
                <div class="pane-header">
                    <label class="pane-header-label">PREVIEW</label>
                    <input id="btn-save-as-image" class="pane-button" type="button" value="Save as Image" />
                </div>
                <div class="preview-frame-container">
                    <iframe id="preview-frame" height="100%" width="100%" frameborder="0" allowfullscreen="true" src=""></iframe>
                </div>
            </div>
        );
    };
}