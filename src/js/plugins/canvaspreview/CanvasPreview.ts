import { PluginBase } from '../../core/PluginBase';
import { IPanes } from '../../core/IPanes';

import './canvaspreview.css';

import Preview from './Preview.jsx';

export class CanvasPreview extends PluginBase {
	registerUIElement(panes: IPanes): void {
		panes.right.push(Preview);
	}

	init(): void {
		this.eventManager.subscribe('CodeEditor.RunCode', (e: any) => this.runCode(e.code));

		const saveAsImageBtn: HTMLInputElement = <HTMLInputElement>document.getElementById('btn-save-as-image');
		saveAsImageBtn.onclick = () => {
			const previewIFrame: HTMLDocument = this.getPreviewIFrameDocument();
			const canvas: HTMLCanvasElement = <HTMLCanvasElement>previewIFrame.getElementById('canvas');
	
			if (canvas) {
				const link = document.createElement('a');
				link.download = 'canvas-image-' + new Date().getTime() + '.png';
				link.href = canvas.toDataURL('image/png');
				link.click();
			}
		};
	}

	runCode(code: string): void {
		const js: string = code;
		const iframeDocument: Document = this.getPreviewIFrameDocument();
		const documentContents: string = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0">' +
			'<meta http-equiv="X-UA-Compatible" content="ie=edge"><title>Preview Document</title><style></style></head><body><canvas id="canvas"></canvas>' + 
			'<script src="js/plugins/canvaspreview/canvasplayground.js"></script><script>' + this.injectRenderTimeScript(js) + '<\/script><\/body><\/html>';
		
		iframeDocument.open();
		iframeDocument.write(documentContents);
		iframeDocument.close();
	}

	getPreviewIFrameDocument(): Document {
		return (<HTMLIFrameElement>document.getElementById('preview-frame')).contentDocument;
	}

	injectRenderTimeScript(js: string): string {
		return 'var $CANVAS_PLAYGROUND_RENDER_START_DATE = new Date;\n' + js +
			'\nparent.document.getElementById("footer-message").innerHTML = "The preview is rendered in " + (new Date - $CANVAS_PLAYGROUND_RENDER_START_DATE) + " ms";';
	}
}