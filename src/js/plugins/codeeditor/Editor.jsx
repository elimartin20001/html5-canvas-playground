import { h, Component } from 'preact';

export default class Editor extends Component {
    render() {
        return (
            <div id="editor">
                <div class="pane-header">
                    <label class="pane-header-label">JS <span class="is-dirty"></span></label>
                    <input id="btn-run" class="pane-button" type="button" value="Run" />
                </div>
                <textarea id="code-input" placeholder="Code goes here..."></textarea>
            </div>
        );
    };
}