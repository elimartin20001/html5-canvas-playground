import { h, Component } from 'preact';

export default class FooterComponent extends Component {
    render() {
        return (
            <footer>
                <div id="footer-message" class="float-left"></div>
                <div class="float-right"><a target="_blank" href="https://icons8.com/icons/set/playground">Playground icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a></div>
            </footer>
        );
    };
}