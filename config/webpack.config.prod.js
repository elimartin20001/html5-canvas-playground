const helpers = require('./helpers'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
      BomPlugin = require('webpack-utf8-bom'),
      webpackConfig = require('./webpack.config.base'),
      MiniCssExtractPlugin = require("mini-css-extract-plugin")
      OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// ensure ts lint fails the build
webpackConfig.module.rules = [...webpackConfig.module.rules,
  {
      test: /\.(scss|sass)$/,
      loader: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
  },
  {
      test: /\.css$/,
      use: [MiniCssExtractPlugin.loader, "css-loader"]
  },
  {
      test: /\.(jpg|png|gif|eot|svg|ttf|woff|woff2)$/,
      loader: 'file-loader'
  }
];

webpackConfig.optimization = {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        extractComments: true,
        uglifyOptions: {
            keep_fnames: true,
            output: {
              comments: false,
              beautify: false
            },
            compress: {
                drop_console: true
            }
          }
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
        cacheGroups: {
            commons: {
                test: /[\\/]node_modules[\\/]/,
                name: "vendors",
                chunks: "all"
            }
        }
    }
};

webpackConfig.plugins = [...webpackConfig.plugins,
new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: [helpers.root('./dist')] }),
new MiniCssExtractPlugin({
  filename: "[name].[hash].css",
  chunkFilename: "[id].[hash].css"
}),
new HtmlWebpackPlugin({
    inject: true,
    template: helpers.root('/src/index.html'),
    minify: {
      collapseWhitespace: true,
      removeComments: true,
      removeRedundantAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      useShortDoctype: true
    }
  }),
new BomPlugin(true)
];

webpackConfig.mode = 'production';

module.exports = webpackConfig;
