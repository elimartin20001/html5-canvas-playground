const helpers = require('./helpers'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      webpackConfig = require('./webpack.config.base');

webpackConfig.module.rules = [...webpackConfig.module.rules,
  {
    test: /\.(scss|sass)$/,
    loader: ['style-loader', 'css-loader', 'sass-loader']
  },
  {
    test: /\.css$/,
    loader: ['style-loader', 'css-loader']
  },
  {
    test: /\.(jpg|png|gif|eot|svg|ttf|woff|woff2)$/,
    loader: 'file-loader'
  }
  ];

webpackConfig.plugins = [...webpackConfig.plugins,
  new HtmlWebpackPlugin({
    inject: true,
    template: helpers.root('/src/index.html')
  })
];

webpackConfig.devServer = {
  port: 8080,
  host: 'localhost',
  historyApiFallback: true,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
    ignore: /node_modules/
  },
  contentBase: './src',
  open: true
};

webpackConfig.mode = 'development';

module.exports = webpackConfig;
