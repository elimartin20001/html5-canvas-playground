﻿var $canvasPlayground = (function () {
  var canvas = document.getElementById("canvas");
 
  return {
    getCanvas: function (w, h) {
      if (w) {
        canvas.width = w;
      }
      
      if (h) {
        canvas.height = h;
      }
      return canvas;
    },

    getContext: function(w, h) {
      var canvas = this.getCanvas(w, h);

      return canvas.getContext("2d");
    }
  };
})();